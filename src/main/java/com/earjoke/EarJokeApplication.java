package com.earjoke;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

public class EarJokeApplication extends Application {

    private static Context sContext;
    private static int sDeviceScreenWidth;
    private static int sDeviceScreenHeight;
    private static int sStatusBarHeight;
    private static float sDeviceScreenDensity;
    private static boolean sIsTablet;
    private static String localDirPath;

    public void onCreate() {
        super.onCreate();

        sContext = getApplicationContext();

        WindowManager wm = (WindowManager)sContext.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR2) {
            sDeviceScreenWidth = display.getWidth();
            sDeviceScreenHeight = display.getHeight();
        } else {
            Point size = new Point();
            display.getSize(size);
            sDeviceScreenWidth = size.x;
            sDeviceScreenHeight = size.y;
        }

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        sDeviceScreenDensity = metrics.density;

        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            sStatusBarHeight = getResources().getDimensionPixelSize(resourceId);
        }

        boolean isHoneycomb = Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
        boolean isTablet = (sContext.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
        sIsTablet = isHoneycomb && isTablet;

        localDirPath = getFilesDir() + "/";
    }

	/* Global helper methods to work with application resources */

    public static Context getAppContext() {
        return sContext;
    }

    public static String getResourceString(int resId) {
        return sContext.getResources().getString(resId);
    }

    public static String getResourceString(int resId, Object... formatArgs) {
        return sContext.getResources().getString(resId, formatArgs);
    }

    public static int getResourceColor(int resId) {
        return sContext.getResources().getColor(resId);
    }

    public static int getResourceDimensionPixelSize(int resId) {
        return sContext.getResources().getDimensionPixelSize(resId);
    }

    public static Bitmap getResourceBitmap(int resId) {
        return BitmapFactory.decodeResource(sContext.getResources(), resId);
    }

    public static Drawable getResourcesDrawable(int resId) {
        return sContext.getResources().getDrawable(resId);
    }

    public static View getInflatedView(int resId) {
        return View.inflate(sContext, resId, null);
    }

    public static int getDeviceWidth() {
        return sDeviceScreenWidth;
    }

    public static int getDeviceHeight() {
        return sDeviceScreenHeight;
    }

    public static int getStatusBarHeight() {
        return sStatusBarHeight;
    }

    public static float getDeviceDensity() {
        return sDeviceScreenDensity;
    }

    public static boolean isTablet() {
        return sIsTablet;
    }

    public static String getLocalDirPath() {
        return localDirPath;
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager inputManager = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (activity.getCurrentFocus() != null && activity.getCurrentFocus().getWindowToken() != null) {
            inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public static int getPxFromDp(float dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, sContext.getResources().getDisplayMetrics());
    }
}