package com.earjoke.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.earjoke.R;

/**
 * Created by user on 24.09.14.
 */
public class CircleView extends View {

    private static final int STROKE_WIDTH = 4;

    private Paint paintWhite;
    private Paint paintOrange;

    private float progress = 0f;

    public CircleView(Context context) {
        super(context);
        init(context);
    }

    public CircleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CircleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        paintWhite = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintWhite.setStyle(Paint.Style.STROKE);
        paintWhite.setStrokeWidth(STROKE_WIDTH);
        paintWhite.setColor(context.getResources().getColor(R.color.circle_white));

        paintOrange = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintOrange.setStyle(Paint.Style.STROKE);
        paintOrange.setStrokeWidth(STROKE_WIDTH);
        paintOrange.setColor(context.getResources().getColor(R.color.circle_orange));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int width = getWidth();
        int height = getHeight();
        if (width != height) {
            throw new IllegalStateException("CircleView must be square");
        }

        RectF rectF = new RectF(STROKE_WIDTH, STROKE_WIDTH,
                width - STROKE_WIDTH, height - STROKE_WIDTH);

        canvas.drawArc(rectF, 270f, 630f, false, paintWhite);

        canvas.drawArc(rectF, 270f, 630 * progress, false, paintOrange);
    }

    public void updateProgress(float progress) {
        if (progress > 1 || progress < 0) {
            throw new IllegalArgumentException("Progress must be in the [0, 1] set");
        }
        this.progress = progress;
        invalidate();
    }
}
