
package com.earjoke.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.earjoke.EarJokeApplication;
import com.earjoke.R;

public class TextViewCustom extends TextView {

    private static Typeface PLAKAT_TYPEFACE = Typeface.createFromAsset(EarJokeApplication.getAppContext().getAssets(), "plakat.ttf");

    public TextViewCustom(Context context) {
        super(context);
    }

    public TextViewCustom(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize(context, attrs);
    }

    public TextViewCustom(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    private void initialize(Context context, AttributeSet attrs) {
        TypedArray attrsArray = context.obtainStyledAttributes(attrs, R.styleable.TextViewCustom);

        final int index = attrsArray.getIndexCount();
        for (int i = 0; i < index; ++i) {
            int attr = attrsArray.getIndex(i);
            switch (attr) {
                case R.styleable.TextViewCustom_font:
                    switch (attrsArray.getInteger(R.styleable.TextViewCustom_font, 1)) {
                        case 1:
                            setTypeface(PLAKAT_TYPEFACE);
                            break;
                    }
                    break;
            }
        }
        attrsArray.recycle();
    }

}
