package com.earjoke;

import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.earjoke.model.enums.ActivityStatus;
import com.earjoke.views.CircleView;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends ActionBarActivity {

    private static final String SENDER_ID = "718496803802";

    private static final String AUDIO_RECORDER_FILE_EXT_MP4 = ".mp4";
    private static final String AUDIO_RECORDER_FOLDER = "AudioRecorder";
    private static final int MAX_DURATION = 1 * 1000 * 60;
    private static final int TIME_ITERATION = 1000;

    private static final int TIME_OUT = 1 * 1000 * 15;

    private ImageButton mImageButton;
    private CircleView mCircleView;
    private ActivityStatus mActivityStatus;

    private MediaRecorder mRecorder;
    private MediaPlayer mPlayer;

    private Timer mTimer;
    private int timeIterations;

    private String fileNameTmp;
    private String regId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    regId = gcm.register(SENDER_ID);
                    Log.i("Debug", "regID = " + regId);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        mCircleView = (CircleView) findViewById(R.id.activity_main_circle);
        mImageButton = (ImageButton) findViewById(R.id.activity_main_btn_joke);
        mImageButton.setOnClickListener(mClickListenerJokeBtn);
        updateActivityState(ActivityStatus.IDLE);

        mTimer = new Timer();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            try {
                String messageStr = extras.getString("message");
                if (messageStr == null) return;
                JSONObject message = new JSONObject(messageStr);
                final String url = message.optString("file");
                String token = message.optString("token");
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        doAudioFileDownload(url);
                        playAudioFile(fileNameTmp);
                    }
                }).start();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private View.OnClickListener mClickListenerJokeBtn = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (mActivityStatus) {
                case RECORDING:
                    stopRecording();
                    break;
                case UPLOADING:
                    break;
                case DOWNLOADING:
                    break;
                case PLAYING:
                    mPlayer.pause();
                    updateActivityState(ActivityStatus.PAUSED);
                    break;
                case IDLE:
                    startRecording();
                    break;
                case PAUSED:
                    mPlayer.start();
                    updateActivityState(ActivityStatus.PLAYING);
                    break;
            }
        }
    };

    private void startRecording() {
        updateActivityState(ActivityStatus.RECORDING);

        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        fileNameTmp = getFilename();
        Log.i("Debug", "file name = " + fileNameTmp);
        mRecorder.setOutputFile(fileNameTmp);
        mRecorder.setMaxDuration(MAX_DURATION);
        mRecorder.setOnErrorListener(errorListener);
        mRecorder.setOnInfoListener(infoListener);
        try {
            mRecorder.prepare();
            mRecorder.start();

            TimerTask timerTask = new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            timeIterations++;
                            mCircleView.updateProgress((float) timeIterations / MAX_DURATION * TIME_ITERATION);
                        }
                    });
                }
            };
            mTimer.schedule(timerTask, 0, TIME_ITERATION);
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getFilename() {
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath, AUDIO_RECORDER_FOLDER);
        if (!file.exists()) {
            file.mkdirs();
        }
        return (file.getAbsolutePath() + "/" + System.currentTimeMillis() + AUDIO_RECORDER_FILE_EXT_MP4);
    }

    private MediaRecorder.OnErrorListener errorListener = new MediaRecorder.OnErrorListener() {
        @Override
        public void onError(MediaRecorder mr, int what, int extra) {
            Toast.makeText(MainActivity.this, "Error: " + what + ", " + extra, Toast.LENGTH_SHORT).show();
        }
    };

    private MediaRecorder.OnInfoListener infoListener = new MediaRecorder.OnInfoListener() {
        @Override
        public void onInfo(MediaRecorder mr, int what, int extra) {
            switch (what) {
                case MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED:
                    Toast.makeText(MainActivity.this, getString(R.string.toast_max_duration_reached), Toast.LENGTH_SHORT).show();
                    stopRecording();
                    break;
                default:
                    Toast.makeText(MainActivity.this, "Warning: " + what + ", " + extra, Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    private void stopRecording() {
        updateActivityState(ActivityStatus.IDLE);

        mTimer.cancel();

        if (mRecorder != null) {
            mRecorder.stop();
            mRecorder.reset();
            mRecorder.release();
            mRecorder = null;

            new Thread(new Runnable() {
                @Override
                public void run() {
                    doAudioFileUpload(fileNameTmp);
                }
            }).start();
        }
    }

    private void doAudioFileUpload(String path) {
        updateActivityState(ActivityStatus.UPLOADING);

        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        DataInputStream inStream = null;

        System.out.println("Inside of doUpload nd path is === " + path);

//        String lineEnd = System.getProperty("line.separator");
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024 * 1024;
        String urlString = "http://cp.ankudinov.org.ua:8000/postnew";
        StringBuilder urlBuilder = new StringBuilder(urlString);
        urlBuilder.append("?token=");
        urlBuilder.append(regId);
        try {
            // ------------------ CLIENT REQUEST
            FileInputStream fileInputStream = new FileInputStream(new File(
                    path));
            // open a URL connection to the Servlet
            URL url = new URL(urlBuilder.toString());
            // Open a HTTP connection to the URL
            conn = (HttpURLConnection) url.openConnection();
            //Setting timeouts
            conn.setConnectTimeout(TIME_OUT);
            conn.setReadTimeout(TIME_OUT);
            // Allow Inputs
            conn.setDoInput(true);
            // Allow Outputs
            conn.setDoOutput(true);
            // Don't use a cached copy.
            conn.setUseCaches(false);
            // Use a post method.
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("Content-Type",
                    "multipart/form-data;boundary=" + boundary);
            dos = new DataOutputStream(conn.getOutputStream());

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"uploadedfile\";filename=\""
                    + path + "\"" + lineEnd);
            dos.writeBytes(lineEnd);
            // create a buffer of maximum size
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];
            // read file and write it into form...
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            while (bytesRead > 0) {
                dos.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }
            // send multipart form data necesssary after file data...
            //////////////////////
//            dos.writeBytes(twoHyphens + boundary + lineEnd);
//            dos.writeBytes("Content-Disposition: text-plain; name=\"deviceToken\"" + lineEnd);
//            dos.writeBytes(lineEnd);
//            dos.writeBytes(regId);
            //////////////////////
            dos.writeBytes(lineEnd);
            dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
            // close streams
            Log.e("Debug", "File is written");
            fileInputStream.close();
            dos.flush();
            dos.close();
            deleteJokeFile(path);
        } catch (MalformedURLException ex) {
            Log.e("Debug", "error: " + ex.getMessage(), ex);
        } catch (IOException ioe) {
            Log.e("Debug", "error: " + ioe.getMessage(), ioe);
        }
        // ------------------ read the SERVER RESPONSE
        try {
            inStream = new DataInputStream(conn.getInputStream());
            String str;

            while ((str = inStream.readLine()) != null) {
                Log.e("Debug", "Server Response " + str);
            }
            inStream.close();
        } catch (IOException ioex) {
            Log.e("Debug", "error: " + ioex.getMessage(), ioex);
        }
        updateActivityState(ActivityStatus.IDLE);
    }

    private void deleteJokeFile(String path) {
        File file = new File(path);
        if (file.exists()) {
            file.delete();
        }
    }

    private void doAudioFileDownload(String urlString) {
        updateActivityState(ActivityStatus.DOWNLOADING);

        int count;
        try {
            URL url = new URL(urlString);
            URLConnection conn = url.openConnection();
            conn.connect();
            // this will be useful so that you can show a tipical 0-100% progress bar
            int lenghtOfFile = conn.getContentLength();

            // downlod the file
            InputStream input = new BufferedInputStream(url.openStream());

            fileNameTmp = getFilename();
            OutputStream output = new FileOutputStream(fileNameTmp);

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
//                publishProgress((int)(total*100/lenghtOfFile));
                output.write(data, 0, count);
            }

            output.flush();
            output.close();
            input.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        updateActivityState(ActivityStatus.IDLE);
    }

    private void playAudioFile(String path) {
        mPlayer = new MediaPlayer();

        try {
            mPlayer.setVolume(1.0f, 1.0f);
            mPlayer.setDataSource(path);
            mPlayer.prepare();

            mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateActivityState(ActivityStatus.IDLE);
                        }
                    });
                }
            });

            mPlayer.start();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateActivityState(ActivityStatus.PLAYING);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateActivityState(ActivityStatus status) {
        mActivityStatus = status;
        switch (mActivityStatus) {
            case RECORDING:
                mImageButton.setImageResource(R.drawable.btn_stop);
                break;
            case UPLOADING:
                break;
            case DOWNLOADING:
                break;
            case PLAYING:
                mImageButton.setImageResource(R.drawable.btn_pause);
                break;
            case IDLE:
                mImageButton.setImageResource(R.drawable.btn_record);
                break;
            case PAUSED:
                mImageButton.setImageResource(R.drawable.btn_play);
                break;
        }
    }
}
