package com.earjoke.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Q on 23.08.2014.
 */
@DatabaseTable(tableName = "joke_table")
public class Joke extends BaseDbModel {

    @DatabaseField(id = true)
    private int id;
    @DatabaseField(dataType = DataType.BYTE_ARRAY)
    private byte[] sound;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public byte[] getSound() {
        return sound;
    }

    public void setSound(byte[] sound) {
        this.sound = sound;
    }
}
