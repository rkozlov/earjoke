package com.earjoke.model.enums;

/**
 * Created by user on 09.09.14.
 */
public enum ActivityStatus {
    RECORDING, UPLOADING, DOWNLOADING, PLAYING, IDLE, PAUSED;
}
